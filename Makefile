MAKEFLAGS += --warn-undefined-variables

# It's necessary to set this because some environments don't link sh -> bash.
SHELL := /usr/bin/env bash

#################################################
# Includes
#################################################

include files/Makefile.docker

##################################################
# HELPER
##################################################

.PHONY: help
help:
	@echo ""
	@echo "****************************************"
	@echo "* 🤖 Management commands"
	@echo "* "
	@echo "* Usage:"
	@echo "* "
	@echo "*  🎉 Short commands 🎉"
	@echo "* "
	@echo "* 📌 make verify"
	@echo "* 📌 make global-requirements"
	@echo "* 📌 make npm-requirements"
	@echo "* 📌 make version"
	@echo "* 📌 make install"
	@echo "* 📌 make scan"
	@echo "* 📌 make release-debug"
	@echo "* 📌 make release"
	@echo "* "
	@echo "****************************************"
	@echo ""

##################################################
# SHORTCUTS
##################################################

verify:
ifeq ($(GITLAB_TOKEN),)
	@echo "ERROR: 🆘 no GITLAB_TOKEN was provided - undefined variable. Exiting." && exit 1
else
	@echo "==> 🎊 We have a GITLAB_TOKEN!"
endif

global-requirements:
	@echo "==> 🌐 Checking global requirements..."
	@command -v git >/dev/null || ( echo "ERROR: 🆘 git binary not found. Exiting." && exit 1)
	@command -v docker >/dev/null || ( echo "ERROR: 🆘 docker binary not found. Exiting." && exit 1)
	@command -v gitleaks >/dev/null || ( echo "ERROR: 🆘 gitleaks binary not found. Exiting." && exit 1)
	@command -v docker-compose >/dev/null || ( echo "ERROR: 🆘 docker-compose binary not found. Exiting." && exit 1)
	@echo "==> ✅ Global requirements are met!"

npm-requirements: global-requirements
	@echo "==> 📜 Checking npm requirements..."
	@command -v npm >/dev/null || ( echo "ERROR: 🆘 npm binary not found. Exiting." && exit 1)
	@echo "==> ✅ Package requirements are met!"

version: npm-requirements
	@echo "==> ✨ Git version: $(shell git --version)"
	@echo "==> ✨ Docker version: $(shell docker --version)"
	@echo "==> ✨ Gitleaks version: $(shell gitleaks --version)"
	@echo "==> ✨ Docker Compose version: $(shell docker-compose --version)"
	@echo "==> ✨ NPM version: $(shell npm --version)"

install: npm-requirements
	@echo "==> 🔥 NPM install packages..."
	@npm install

scan: global-requirements
	@echo "==> 🔒 Scan git repo for secrets..."
	@npm run secrets

release-debug: install verify
	@echo "==> 📦 Runnig release debug..."
	@npm run release-debug

release: install verify
	@echo "==> 📦 Runnig release..."
	@npm run release

#################################################
# DOCKER SHORTCUTS
#################################################

.PHONY: init
init:
	sudo mkdir -p /opt/data/gitlab/config
	sudo mkdir -p /opt/data/gitlab/data
	sudo mkdir -p /opt/data/gitlab/logs
	sudo mkdir -p /opt/data/gitlab-runner/config
	sudo mkdir -p /opt/data/minio/data
	sudo mkdir -p /opt/data/grafana/data
	sudo mkdir -p /opt/data/prometheus/data
	docker-compose up -d
	@echo "# ----------------------------------"
	@echo "# gitlab : http://localhost:8929"
	@echo "# registry : http://localhost:5001/v2/"
	@echo "# minio : http://localhost:9000"
	@echo "# ----------------------------------"

.PHONY: log
log:
	docker-compose logs -f

.PHONY: stop
stop:
	docker-compose stop

.PHONY: start
start:
	docker-compose start

.PHONY: register-one
register-one:
	@echo "# ----------------------------------"
	@echo "# register GitLab Runner One"
	@echo "# ----------------------------------"
	docker-compose exec gitlab-runner-one \
	  gitlab-runner register \
	    --non-interactive \
			--url=http://gitlab:8929/ \
			--clone-url=http://gitlab:8929 \
			--registration-token=stefanini@10 \
			--tag-list=docker,local \
			--executor=docker \
			--locked=false \
			--description="Local shared runner Docker-in-Docker - I" \
			--docker-image=alpine:latest \
			--docker-network-mode=gitlab-network \
			--docker-volumes=/var/run/docker.sock:/var/run/docker.sock \
			--cache-type=s3 \
			--cache-path=gitlab-runner \
			--cache-shared=true \
			--cache-s3-server-address=minio:9000 \
			--cache-s3-access-key=minio \
			--cache-s3-secret-key=stefanini@10 \
			--cache-s3-bucket-name=gitlab \
			--cache-s3-insecure=true

.PHONY: register-two
register-two:
	@echo "# ----------------------------------"
	@echo "# register GitLab Runner Two"
	@echo "# ----------------------------------"
	docker-compose exec gitlab-runner-two \
			gitlab-runner register \
				--non-interactive \
				--url=http://gitlab:8929/ \
				--clone-url=http://gitlab:8929 \
				--registration-token=stefanini@10 \
				--tag-list=devops,internal \
				--executor=docker \
				--locked=false \
				--description="Local shared runner Docker-in-Docker - II" \
				--docker-image=alpine:latest \
				--docker-network-mode=gitlab-network \
				--docker-volumes=/var/run/docker.sock:/var/run/docker.sock \
				--cache-type=s3 \
				--cache-path=gitlab-runner \
				--cache-shared=true \
				--cache-s3-server-address=minio:9000 \
				--cache-s3-access-key=minio \
				--cache-s3-secret-key=stefanini@10 \
				--cache-s3-bucket-name=gitlab \
				--cache-s3-insecure=true

.PHONY: register
register: register-one register-two

.PHONY: clean
clean:
	docker-compose rm -f -s
	@for container in $$(docker ps -q -a --filter exited=0 --filter name=runner-.*-project-.*-concurrent-.*-cache-.*$$); do docker rm $$container; done;

.PHONY: remove
remove:
	docker-compose rm -f -s
	@for container in $$(docker ps -q -a --filter exited=0 --filter name=runner-.*-project-.*-concurrent-.*-cache-.*$$); do docker rm $$container; done;
	sudo rm -rf /opt/data/*
