# Semantic Versioning Changelog

## 1.0.0 (2021-07-17)


### :sparkles: News

* **add:** docker compose gitlab ecosystem ([b4a4caa](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/b4a4caad6996ba6e1f8844305702dbd84dabab72))
* **add:** git hooks commit msg and pre commit ([d8694e3](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/d8694e3d6f7138ab27ca1c803ef20f5a2d835b3c))
* **setup:** initial commit project ([0450109](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/045010915bb6cdbb3dc2a900b4a099081d19e8f3))


### :memo: Docs

* **feat:** add description ([464f361](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/464f361e538457ddd3f976b6a779442ca18d3444))
* **feat:** add steps ([62bd13e](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/62bd13ea5f1b7f191e3650225a86b975457403c9))
* **feat:** pretty design ([751edfd](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/751edfd4d8043cbb12dd5a0fc5460311bc9c849f))
* **fix:** gif gitlab ([59a1909](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/59a19096d55c7ef9671278d7669882e641f7163b))
* **fix:** menu topics ([80659bc](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/80659bc7459b32c727aeeae74d0b0b746544a340))
* **fix:** more to right ([96c25ff](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/96c25ff31cfaa51e64610caa51b37f748ca47ac6))
* **fix:** position ([5669df4](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/5669df4ac6a981ef1ea470fc99916b174ea137fe))
* add pretty readme ([6cc7a70](https://gitlab.com/bootcamp-stefanini/gitlab-ecosystem/commit/6cc7a700b0675d7a0393570c1a2cbbf1a3488851))
